# Installation of FsPages

**WARNING** you **need** to install FsPages on the Gitlab server: it clones repositories with `file:///` URLs.

## Dependencies

### Some necessary packages

```
apt-get install build-essential git
```

### Carton

Perl dependencies manager, it will get what you need, so don't bother for Perl modules dependencies (but you can read the file `cpanfile` if you want).

Best way to install Carton:

```shell
sudo cpan Carton
```

## Installation

After installing Carton:

```
cd /opt
git clone https://framagit.org/luc/fs-pages.git
cd fs-pages
carton install
cp fs_pages.conf.template fs_pages.conf
vi fs_pages.conf
# Choose the user which owns the gitlab repositories folder
chown -R git: .
```

## Configuration

The `fs_pages.conf.template` is self-documented.

## Starting FsPages from command line

```
carton exec hypnotoad script/fs_pages
```

## Starting the minion worker from command line

```
carton exec script/fs_pages minion worker
```

## Systemd

```
cp utilities/fs-pages.service /etc/systemd/system/
cp utilities/fs-pages-minion.service /etc/systemd/system/
# Change User, WorkingDirectory, PIDFile and the absolute path to carton to match your installation
vi /etc/systemd/system/fs-pages.service /etc/systemd/system/fs-pages-minion.service
systemctl enable fs-pages.service fs-pages-minion.service
systemctl start fs-pages.service fs-pages-minion.service
```

## Nginx

If you installed Gitlab from scratch, without the omnibus packages:

```
cp utilities/fs-pages.nginx.conf /etc/nginx/sites-available/fs-pages.conf
ln -s /etc/nginx/sites-available/fs-pages.conf /etc/nginx/sites-enabled/fs-pages.conf
# Adapt the configuration: https, server_name, paths of your pages and master folders, etc
vi /etc/nginx/sites-available/fs-pages.conf
nginx -t
nginx -s reload
```

If you installed Gitlab from omnibus packages:

```
vi /etc/gitlab/gitlab.rb
```

Modify, if needed, the `nginx['custom_nginx_config']` to be able to add nginx configuration. My advice:

```
nginx['custom_nginx_config'] = "include /var/opt/gitlab/nginx/conf/conf.d/*.conf;"
```

Ensure that `/var/opt/gitlab/nginx/conf/conf.d/`, then:

```
cp utilities/fs-pages.nginx.conf /var/opt/gitlab/nginx/conf/conf.d/fs-pages.conf
# Adapt the configuration: https, server_name, paths of your pages and master folders, etc
vi /var/opt/gitlab/nginx/conf/conf.d/fs-pages.conf
gitlab-ctl restart nginx
```

And that's finished :-)

## Update

```
git pull
carton install
systemctl reload fs-pages.service
systemctl restart fs-pages-minion.service
```
